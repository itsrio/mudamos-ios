//
//  AppDelegate.h
//  mudamos
//
//  Created by Luiz Aires Soares on 11/6/15.
//  Copyright © 2015 Inventos Digitais. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

