//
//  HomeViewController.m
//  mudamos
//
//  Created by Luiz Aires Soares on 11/7/15.
//  Copyright © 2015 Inventos Digitais. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnLogin.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnLogin.layer.cornerRadius = 2.0f;
    self.btnLogin.layer.borderWidth = 2.0f;
    self.btnLogin.layer.masksToBounds = YES;
    
    self.btnRegister.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnRegister.layer.cornerRadius = 2.0f;
    self.btnRegister.layer.borderWidth = 2.0f;
    self.btnRegister.layer.masksToBounds = YES;
    
    self.btnExplore.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnExplore.layer.cornerRadius = 2.0f;
    self.btnExplore.layer.borderWidth = 2.0f;
    self.btnExplore.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
