//
//  HomeViewController.h
//  mudamos
//
//  Created by Luiz Aires Soares on 11/7/15.
//  Copyright © 2015 Inventos Digitais. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnExplore;

@end
